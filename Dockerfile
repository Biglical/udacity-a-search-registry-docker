FROM ubuntu:focal

RUN apt-get -y update 
RUN DEBIAN_FRONTEND=noninteractive TZ=Etc/UTC apt-get -y install tzdata
&& apt-get -y  install build-essential cmake libcairo2-dev libgraphicsmagick1-dev libpng-dev

RUN mkdir /git &&  cd /git && git clone --recurse-submodules https://github.com/cpp-io2d/P0267_RefImpl  && cd P0267_RefImpl && mkdir Debug && cd Debug && cmake --config Debug "-DCMAKE_BUILD_TYPE=Debug" .. && cmake --build . && cd P0267_RefImpl/P0267_RefImpl/ && make install

RUN cd /tmp && mkdir registry && cd registry && wget http://nsis.sourceforge.net/mediawiki/images/4/47/Registry.zip && 7z x Registry.zip && cd /usr/share/nsis && cp -a /tmp/registry/Desktop/Include/* Include/ && cp -a /tmp/registry/Desktop/Plugin/* Plugins/x86-ansi/ && rm -rf /tmp/registry
